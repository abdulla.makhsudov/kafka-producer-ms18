package az.ingress.kafkaproducerms18.controller;

import static org.springframework.http.HttpStatus.OK;

import az.ingress.kafkaproducerms18.model.Student;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/publish")
@RequiredArgsConstructor
public class ProducerController {

    private final KafkaTemplate<String, Object> kafkaTemplate;


    @GetMapping
    public ResponseEntity<Void> publish(@RequestParam String message) {
        kafkaTemplate.send("ms18Topic", "4", message);
        return new ResponseEntity<>(OK);
    }

    @PostMapping
    public ResponseEntity<Void> publishObject(@RequestBody Student student) {
        kafkaTemplate.send("ms18TopicObject", null, student);
        return new ResponseEntity<>(OK);
    }
}
